<?php

require_once __DIR__ . '/m161213_065532_install_main_city_data.php';

use yii\db\Migration;

class m170109_140759_add_cities_data extends Migration
{
    public function up()
    {
        $tableName = m161213_065532_install_main_city_data::$cityTableName;
        $cities = require __DIR__ . '/cities_data.php';

        foreach ($cities as $city) {
            $this->insert($tableName, [
                'id' => $city['id'],
                'id_parent' => $city['id_parent'],
                'meta_title' => $city['name'],
                'name' => $city['name'],
                'name_small' => $city['name_small'],
                'url' => $city['url'],
                'geo_point' => $city['geo_point'],
                'active' => \amd_php_dev\module_city\models\City::ACTIVE_ACTIVE,
                'created_at' => $time = time(),
                'updated_at' => $time,
            ]);
        }
    }

    public function down()
    {
        echo "m170109_140759_add_cities_data cannot be reverted.\n";

        return false;
    }
}
