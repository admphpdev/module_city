<?php

require_once __DIR__ . '/m161213_065532_install_main_city_data.php';

use amd_php_dev\yii2_components\migrations\Migration;
use amd_php_dev\yii2_components\migrations\generators\Page;

class m170111_091344_add_regions_data extends Migration
{
    public static $rerionTableName                    = '{{%city_region}}';

    protected $cityTableName;

    public function safeUp()
    {
        if (!\yii::$app->getDb()->getSchema()->getTableSchema(self::$rerionTableName)) {
            $generator = new Page($this, self::$rerionTableName);
            $generator->create();
        }
        $this->addData();
    }

    public function safeDown()
    {
        echo "m170111_091344_add_regions_data cannot be reverted.\n";

        return false;
    }

    protected function addData()
    {
        $schema = \yii::$app->getDb()->getSchema();
        $this->cityTableName = \m161213_065532_install_main_city_data::$cityTableName;

        $regionsData = require_once __DIR__ . '/regions_data.php';

        if (!$schema->getTableSchema($this->cityTableName)->getColumn('id_region')) {
            $this->addColumn($this->cityTableName, 'id_region', $this->integer());
        }

        if (!$schema->getTableSchema($this->cityTableName)->getColumn('is_capital')) {
            $this->addColumn($this->cityTableName, 'is_capital', $this->boolean()->defaultValue(0));
        }

        foreach ($regionsData as $region) {
            $this->insert(self::$rerionTableName, [
                'id' => $region['id'],
                'meta_title' => $region['name'],
                'name' => $region['name'],
                'url' => \amd_php_dev\yii2_components\helpers\TextHelper::str2url($region['name']),
                'active' => \amd_php_dev\yii2_components\models\Page::ACTIVE_ACTIVE,
                'created_at' => $time = time(),
                'updated_at' => $time,
                'image_full' => $region['flag'],
                'image_small' => $region['gerb'],
            ]);

            $capital = \Yii::$app->db->createCommand("SELECT * FROM {$this->cityTableName} WHERE `name` = :capital")
                ->bindValues(['capital' => $region['capital']])
                ->queryOne();

            if (!empty($capital['id'])) {
                // Установить is_capital
                // Установить id_region
                $this->update($this->cityTableName, ['is_capital' => 1, 'id_region' => (int) $region['id']], '`id` = ' . (int) $capital['id']);

                // Для всех у кого id_parent = id установить id_region = id_region
                $this->update($this->cityTableName, ['id_region' => (int) $region['id']], '`id_parent` = ' . (int) $capital['id']);
            }
        }

        // Удалить поле id_parent
        if ($schema->getTableSchema($this->cityTableName)->getColumn('id_parent')) {
            $this->dropIndex('idx_' . $this->getFullTableName($this->cityTableName) . '_' . 'id_parent', $this->cityTableName);
            $this->dropColumn($this->cityTableName, 'id_parent');
        }
    }
}
