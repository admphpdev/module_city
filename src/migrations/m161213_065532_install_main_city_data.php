<?php

use amd_php_dev\yii2_components\migrations\Migration;
use amd_php_dev\yii2_components\migrations\generators\Page;
use amd_php_dev\yii2_components\migrations\generators\Option;
use amd_php_dev\yii2_components\migrations\generators\OptionValue;
use amd_php_dev\yii2_components\migrations\generators\OptionGroup;
use amd_php_dev\yii2_components\migrations\generators\Generator;

class m161213_065532_install_main_city_data extends Migration
{
    public static $cityTableName                    = '{{%city_city}}';
    public static $cityOptionTableName              = '{{%city_city_option}}';
    public static $cityOptionValueTableName         = '{{%city_city_option_value}}';
    public static $cityOptionGroupTableName         = '{{%city_city_option_group}}';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $generator = new Page($this, self::$cityTableName);
        $generator->additionalColumns['geo_point']  = $this->string(255);
        $generator->additionalColumns['id_parent']  = $this->integer();
        $generator->addIndex('id_parent');
        $generator->create();

        $generator = new Option($this, self::$cityOptionTableName);
        $generator->additionalColumns['id_group']   = $this->integer();
        $generator->addIndex('id_group');
        $generator->create();

        $generator = new OptionValue($this, self::$cityOptionValueTableName);
        $generator->create();

        $generator = new OptionGroup($this, self::$cityOptionGroupTableName);
        $generator->create();
    }

    public function safeDown()
    {
        $this->dropTable(self::$cityTableName);
        $this->dropTable(self::$cityOptionTableName);
        $this->dropTable(self::$cityOptionValueTableName);
        $this->dropTable(self::$cityOptionGroupTableName);
    }

}
