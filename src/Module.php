<?php

namespace amd_php_dev\module_city;

/**
 * city module definition class
 */
class Module extends \amd_php_dev\yii2_components\modules\Module
{
    //public $layout      = '@app/views/layouts/default';
    use \amd_php_dev\yii2_components\modules\ComposerModuleTrait;

    protected $_urlRules = [
        [
            'rules' => [
                ['class' => '\amd_php_dev\module_city\urlRules\Rules']
            ],
            'append' => true,
        ],
    ];

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'amd_php_dev\module_city\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->modules = [
            'admin' => [
                'class' => 'amd_php_dev\module_city\modules\admin\Module',
            ],
        ];

        // custom initialization code goes here
    }

    //public static function getMenuItems() {
    //    return [
    //        'section' => 'city',
    //        'items' => [
    //            [
    //                'label' => 'city',
    //                'items' => [
    //                    ['label' => 'label', 'url' => ['']],
    //                ]
    //            ]
    //        ],
    //    ];
    //}
}
