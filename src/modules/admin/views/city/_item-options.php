<?php
/* @var $this yii\web\View */
/* @var $model amd_php_dev\module_city\models\City */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="options-set" id="item-options">
    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
        'model'     => $model,
        'attribute' => \amd_php_dev\module_city\models\City::ATTR_OPTIONS,
        'label'     => true,
        'form'      => $form,
    ]); ?>
</div>

