<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model amd_php_dev\module_city\models\City */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="city-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype'=>'multipart/form-data'],
        'id' => 'item-form',
    ]); ?>

    <?= Tabs::widget([
        'items' => [
            [
                'label' => 'Основное',
                'content' => $this->render('_item-fields', ['model' => $model, 'form' => $form]),
                'active' => true
            ],
            [
                'label' => 'Дополнительно',
                'content' => $this->render('_item-options', ['model' => $model, 'form' => $form]),
            ],
        ],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
