<?php

namespace amd_php_dev\module_city\modules\admin;

/**
 * admin module definition class
 */
class Module extends \amd_php_dev\yii2_components\modules\Admin
{
    //public $layout      = '@app/views/layouts/default';

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'amd_php_dev\module_city\modules\admin\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        //$this->modules = [
        //
        //];

        // custom initialization code goes here
    }

    public static function getMenuItems() {
        return [
            'section' => 'admin',
            'items' => [
                [
                    'label' => 'Города',
                    'items' => [
                        ['label' => 'Регионы', 'url' => ['/city/admin/region/index']],
                        ['label' => 'Города', 'url' => ['/city/admin/city/index']],
                        ['label' => 'Храктеристики', 'url' => ['/city/admin/city-option/index']],
                        ['label' => 'Группы характеристик', 'url' => ['/city/admin/city-option-group/index']],
                    ]
                ]
            ],
        ];
    }
}
