<?php
/**
 * Created by PhpStorm.
 * User: v-11
 * Date: 11.01.2017
 * Time: 14:37
 */

namespace amd_php_dev\module_city\models;

use Yii;
use amd_php_dev\yii2_components\widgets\form\SmartInput;
use amd_php_dev\yii2_components\models\SmartQuery;

/**
 * This is the model class for table "{{%city_region}}".
 *
 * @property integer $id
 * @property integer $active
 * @property integer $priority
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $author
 * @property string $name
 * @property string $name_small
 * @property string $url
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $text_small
 * @property string $text_full
 * @property string $links
 * @property string $snipets
 * @property string $image_small
 * @property string $image_full
 */
class Region extends \amd_php_dev\yii2_components\models\Page
{
    const IMAGES_URL_ALIAS = '@web/data/city/region/images/all/';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%city_region}}';
    }

    /**
     * @inheritdoc
     * @return SmartQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SmartQuery(get_called_class());
    }
}