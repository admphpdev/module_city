<?php

namespace amd_php_dev\module_city\models;

use Yii;
use amd_php_dev\yii2_components\widgets\form\SmartInput;

/**
 * This is the model class for table "{{%city_city}}".
 *
 * @property integer $id_region
 * @property integer $id
 * @property integer $is_capital
 * @property integer $active
 * @property integer $priority
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $author
 * @property string $geo_point
 * @property string $name
 * @property string $name_small
 * @property string $url
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $text_small
 * @property string $text_full
 * @property string $links
 * @property string $snipets
 * @property string $image_small
 * @property string $image_full
 */
class City extends \amd_php_dev\yii2_components\models\Page
{
    const IMAGES_URL_ALIAS = '@web/data/city/images/all/';

    const ATTR_OPTIONS = 'optionValues';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%city_city}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'optionsManager' => [
                'class' => \amd_php_dev\yii2_components\behaviors\OptionBehavior::className(),
                'setableAttribute' => static::ATTR_OPTIONS,
                'optionsRelation' => 'optionsRelation',
                'optionValuesRelation' => 'optionValuesRelation',
                'optionGroupsRelation' => 'optionGroupsRelation',
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function getActiveArray()
    {
        //return \yii\helpers\ArrayHelper::merge(parent::getActiveArray(), [
        //
        //]);
        return parent::getActiveArray();
    }

    /**
     * @inheritdoc
     */
    public function getItemUrl() {
        if ($this->isNewRecord)
            return false;

        //return Url::to(['', 'url' => $this->url]);
        return '';
    }

    /**
     * @inheritdoc
     */
    public function getInputType($attribute)
    {
        $result = null;

        switch ($attribute) {
            case 'id_region' :
                $result = SmartInput::TYPE_SELECT;
                break;
            case 'is_capital' :
                $result = SmartInput::TYPE_CHECKBOX;
                break;
            case static::ATTR_OPTIONS :
                $result = SmartInput::TYPE_OPTIONS;
                break;
            default:
                $result = parent::getInputType($attribute);
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function getInputData($attribute)
    {
        $result = null;

        switch ($attribute) {
            case 'id_region' :
                $result = ['0' => 'Без региона'];
                $query = $this->getRegionRelation()->clean();
                $data = $query->asArray()->all();

                foreach ($data as $item) {
                    $result[$item['id']] = $item['id'] . ' - ' . $item['name'];
                }
                break;
            case static::ATTR_OPTIONS :
                $result = $result = $this->getBehavior('optionsManager');
                break;
            default:
                $result = parent::getInputData($attribute);
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function getInputOptions($attribute)
    {
        $result = null;

        switch ($attribute) {
            default:
                $result = parent::getInputOptions($attribute);
        }

        return $result;
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return \yii\helpers\ArrayHelper::merge(parent::rules(), [
            [['id_region'], 'required'],
            [['id_region'], 'integer'],
            [['id_region'], 'checkCapitalRegionChange'],
            [['is_capital'], 'boolean'],
            [['is_capital'], 'default', 'value' => 0],
            [['url'], 'unique'],
            [['geo_point'], 'string', 'max' => 255],
            [[static::ATTR_OPTIONS], 'safe'],
        ]);
        /*return [
            [['id_region', 'active', 'priority', 'created_at', 'updated_at', 'author'], 'integer'],
            [['text_small', 'text_full', 'links', 'snipets'], 'string'],
            [['name', 'name_small', 'url', 'meta_title', 'meta_keywords', 'meta_description', 'image_small', 'image_full'], 'string', 'max' => 255],
        ];*/
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return \yii\helpers\ArrayHelper::merge(parent::attributeLabels(), [
            'id_region' => 'Регион',
            'is_capital' => 'Столица региона',
            static::ATTR_OPTIONS => 'Характеристики',
        ]);
    }

    /**
     * @inheritdoc
     * @return CityQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CityQuery(get_called_class());
    }

    public function getRegionRelation()
    {
        return $this->hasOne(Region::className(), ['id' => 'id_region']);
    }

    /**
     * @return \amd_php_dev\yii2_components\models\PageQuery
     */
    public function getOptionGroupsRelation()
    {
        return CityOptionGroup::find()
            ->defaultScope()
            ->defaultSort()
            ->indexBy('id');
    }

    /**
     * @return \yii\db\Query
     */
    public function getOptionValuesRelation()
    {
        $query = new \yii\db\Query();
        return $query->select('*')
            ->from('{{%city_city_option_value}}')
            ->where('id_item = ' . (int) $this->id)
            ->indexBy('id_option');
    }

    /**
     * @return \amd_php_dev\yii2_components\models\OptionQuery
     */
    public function getOptionsRelation()
    {
        return CityOption::find()
            ->defaultScope()
            ->defaultSort()
            ->indexBy('id');
    }

    public function checkCapitalRegionChange()
    {
        if (
            !empty($this->getOldAttribute('is_capital'))
            && $this->getOldAttribute('id_region') != $this->id_region
        ) {
            $this->addError('id_region', 'Невозможно изменить регион у столицы!');
        }

        return true;
    }

    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);

        // Может быть только одна столица региона
        if (isset($changedAttributes['is_capital']) && $this->is_capital) {
            \Yii::$app->db->createCommand()
                ->update(
                    self::tableName(),
                    ['is_capital' => 0],
                    'is_capital = 1 AND id_region = ' . (int) $this->id_region . ' AND id != ' . (int) $this->id
                )
                ->execute();
        }
    }
}
