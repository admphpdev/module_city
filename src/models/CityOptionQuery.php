<?php

namespace amd_php_dev\module_city\models;

/**
 * This is the ActiveQuery class for [[CityOption]].
 *
 * @see CityOption
 */
class CityOptionQuery extends \amd_php_dev\yii2_components\models\OptionQuery
{

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        //return ArrayHelper::merge(parent::behaviors(), [
        //
        //]);
        return parent::behaviors();
    }

    /**
     * @inheritdoc
     * @return CityOption[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CityOption|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
