<?php

namespace amd_php_dev\module_city\models;

use Yii;
use amd_php_dev\yii2_components\widgets\form\SmartInput;

/**
 * This is the model class for table "{{%city_city_option}}".
 *
 * @property integer $id_group
 * @property integer $id
 * @property integer $active
 * @property integer $priority
 * @property integer $in_filter
 * @property integer $required
 * @property string $code
 * @property string $image
 * @property string $type
 * @property string $name
 * @property string $variants
 * @property string $default
 * @property string $description
 */
class CityOption extends \amd_php_dev\yii2_components\models\Option
{
    const IMAGES_URL_ALIAS = '@web/data/city/option/images/all/';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%city_city_option}}';
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        //return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
        //
        //]);
        return parent::behaviors();
    }

    /**
    * @inheritdoc
    */
    public static function getActiveArray()
    {
        //return \yii\helpers\ArrayHelper::merge(parent::getActiveArray(), [
        //
        //]);
        return parent::getActiveArray();
    }

    /**
    * @inheritdoc
    */
    public function getItemUrl() {
        if ($this->isNewRecord)
            return false;

        //return Url::to(['', 'url' => $this->url]);
        return '';
    }

    /**
    * @inheritdoc
    */
    public function getInputType($attribute)
    {
        $result = null;

        switch ($attribute) {
            case 'id_group' :
                $result = SmartInput::TYPE_SELECT;
                break;
            default:
                $result = parent::getInputType($attribute);
        }

        return $result;
    }

    /**
    * @inheritdoc
    */
    public function getInputData($attribute)
    {
        $result = null;

        switch ($attribute) {
            case 'id_group' :
                $result = ['0' => 'Без группы'];

                $data = $this->getGroupRelation()->clean()->asArray()->all();

                foreach ($data as $item) {
                    $result[$item['id']] = $item['id'] . ' - ' . $item['name'];
                }

                break;
            default:
                $result = parent::getInputData($attribute);
        }

        return $result;
    }

    /**
    * @inheritdoc
    */
    public function getInputOptions($attribute)
    {
        $result = null;

        switch ($attribute) {
            default:
                $result = parent::getInputOptions($attribute);
        }

        return $result;
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return \yii\helpers\ArrayHelper::merge(parent::rules(), [
            [['id_group'], 'integer']
        ]);
        /*return [
            [['id_group', 'active', 'priority', 'in_filter', 'required'], 'integer'],
            [['variants', 'default', 'description'], 'string'],
            [['code', 'image', 'type', 'name'], 'string', 'max' => 255],
        ];*/
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return \yii\helpers\ArrayHelper::merge(parent::attributeLabels(), [
            'id_group' => 'Группа характеристик',
        ]);
    }

    /**
     * @inheritdoc
     * @return CityOptionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CityOptionQuery(get_called_class());
    }

    public function getGroupRelation()
    {
        return $this->hasOne(
            CityOptionGroup::className(),
            ['id' => 'id_group']
        );
    }
}
